# axfive-imgui

Simple imgui wrapper around imgui-sys (maybe later cimgui), used for working
with imgui and windowing from Rust in a way that is useful for axfive projects.  

This may have some memory safety issues, but most of that is hopefully fixed now
via lifetimes.  My understanding of lifetimes are still somewhat limited, so
you might not want to bet on this being a sure thing.
