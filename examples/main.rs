use axfive_imgui::events::Event;
use axfive_imgui::{self as imgui, glium, imgui_sys, static_cstr, Context};
use glium::Surface;
use std::cell::RefCell;
use std::error::Error;
use std::pin::Pin;

fn main() -> Result<(), Box<dyn Error>> {
    let mut events_loop = glium::glutin::EventsLoop::new();
    let mut running = true;
    let wb = glium::glutin::WindowBuilder::new();
    let cb = glium::glutin::ContextBuilder::new();
    let mut display = glium::Display::new(wb, cb, &events_loop)?;
    let mut context = Context::new(&display)?;
    let mut file_dialog = imgui::FileDialog::new(".")?;
    let buffer = Pin::new(Box::new(RefCell::new(vec![0])));
    while running {
        let mut frame = context.start_frame(&mut events_loop, &mut display);
        for event in frame.events() {
            match event {
                Event::Quit => {
                    return Ok(());
                }
                _ => (),
            }
        }

        {
            let main_menu_bar = imgui::MainMenuBar::new(&mut frame);
            if main_menu_bar.open() {
                let menu = imgui::Menu::new(&mut frame, static_cstr!("File"), true);
                if menu.open() {
                    if imgui::menu_item(
                        &mut frame,
                        static_cstr!("Quit"),
                        Some(static_cstr!("Ctrl-Q")),
                        false,
                        true,
                    ) {
                        running = false;
                    }
                }
            }
        }

        if !running {
            return Ok(());
        }

        imgui::show_demo_window(&mut running);

        {
            let window = imgui::Window::new(
                &mut frame,
                static_cstr!("Test Window"),
                &mut running,
                imgui_sys::ImGuiWindowFlags_HorizontalScrollbar as i32,
            );
            if window.open() {
                imgui::text(&mut frame, "This is test text");
                let show = imgui::button(&mut frame, static_cstr!("Open dialog"), (0.0, 0.0));
                file_dialog.render(&mut frame, show);
            }

            let child = imgui::Child::new(
                &mut frame,
                static_cstr!("Child Window"),
                (0.0, 0.0),
                true,
                imgui_sys::ImGuiWindowFlags_HorizontalScrollbar as i32,
            );

            if child.open() {
                imgui::input_text(
                    &mut frame,
                    static_cstr!("Input"),
                    &buffer,
                    imgui_sys::ImGuiInputTextFlags_CallbackResize as i32,
                );
            }
        }

        let mut target = display.draw();
        target.clear_color(0.5, 0.5, 0.5, 1.0);
        frame.render(&mut display, &mut target)?;
        target.finish()?;
    }
    Ok(())
}
