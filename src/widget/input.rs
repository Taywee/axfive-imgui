use crate::context::frame::Frame;
use imgui_sys::*;
use std::cell::RefCell;
use std::ffi::{c_void, CStr};
use std::ops::Deref;
use std::os::raw::{c_char, c_int};
use std::pin::Pin;

unsafe extern "C" fn resize_input_text_buffer(data: *mut ImGuiInputTextCallbackData) -> c_int {
    let data = &mut *data;
    if data.EventFlag == ImGuiInputTextFlags_CallbackResize as i32 {
        let buf = &*(data.UserData as *const RefCell<Vec<c_char>>);
        let mut vec = buf.borrow_mut();
        vec.resize(data.BufSize as usize, 0);
        data.Buf = vec.as_mut_ptr();
    }
    0
}

/// Resizes its input as necessary.  If so, you must add the CallbackResize flag into the flags
/// list.
/// The buf's type is to allow the user to access it mutably during a frame but to ensure that it
/// is only obtained mutably once and maintains its memory location for a while.
pub fn input_text<'a, 'b, 'c, P>(
    _: &mut Frame<'a>,
    label: &'b CStr,
    buf: &'c Pin<P>,
    flags: ImGuiInputTextFlags,
) -> bool
where
    'b: 'a,
    'c: 'a,
    P: Deref<Target = RefCell<Vec<c_char>>>,
{
    unsafe {
        // Let the RefCell borrow expire so a callback doesn't panic
        let (ptr, len) = {
            let mut vec = Pin::into_inner(buf.as_ref()).borrow_mut();
            (vec.as_mut_ptr(), vec.len())
        };
        imgui_sys::igInputText(
            label.as_ptr(),
            ptr,
            len,
            flags,
            Some(resize_input_text_buffer),
            Pin::into_inner(buf.as_ref()) as *const RefCell<Vec<c_char>> as *const c_void
                as *mut c_void,
        )
    }
}
