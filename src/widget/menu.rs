use crate::context::frame::Frame;
use imgui_sys::*;
use std::ffi::CStr;
use std::marker::PhantomData;
use std::ptr;

pub struct MainMenuBar<'a> {
    open: bool,
    phantom: PhantomData<&'a ()>,
}

impl<'a> MainMenuBar<'a> {
    pub fn new<'b>(_: &mut Frame<'b>) -> MainMenuBar<'a>
    where
        'b: 'a,
    {
        MainMenuBar {
            open: unsafe { igBeginMainMenuBar() },
            phantom: PhantomData,
        }
    }

    /// Tells whether this is "open" or not, ie. the return value of the BeginMainMenuBar call.
    /// This is used to determine whether to start establishing contents.
    pub fn open(&self) -> bool {
        self.open
    }
}

impl<'a> Drop for MainMenuBar<'a> {
    fn drop(&mut self) {
        if self.open {
            unsafe {
                igEndMainMenuBar();
            }
        }
    }
}

pub struct Menu<'a> {
    open: bool,
    phantom: PhantomData<&'a ()>,
}

impl<'a> Menu<'a> {
    pub fn new<'b, 'c>(_: &mut Frame<'b>, title: &'c CStr, enabled: bool) -> Menu<'a>
    where
        'c: 'b,
        'b: 'a,
    {
        Menu {
            open: unsafe { igBeginMenu(title.as_ptr(), enabled) },
            phantom: PhantomData,
        }
    }

    /// Tells whether this is "open" or not, ie. the return value of the BeginMenu call.
    /// This is used to determine whether to start establishing contents.
    pub fn open(&self) -> bool {
        self.open
    }
}

impl<'a> Drop for Menu<'a> {
    fn drop(&mut self) {
        if self.open {
            unsafe {
                igEndMenu();
            }
        }
    }
}

pub fn menu_item<'a, 'b, 'c>(
    _: &mut Frame<'a>,
    label: &'b CStr,
    shortcut: Option<&'c CStr>,
    selected: bool,
    enabled: bool,
) -> bool
where
    'b: 'a,
    'c: 'a,
{
    let shortcut_ptr = match shortcut {
        Some(s) => s.as_ptr(),
        None => ptr::null(),
    };

    unsafe { igMenuItemBool(label.as_ptr(), shortcut_ptr, selected, enabled) }
}

pub fn menu_item_toggle<'a, 'b, 'c>(
    _: &mut Frame<'a>,
    label: &'b CStr,
    shortcut: Option<&'c CStr>,
    selected: &mut bool,
    enabled: bool,
) -> bool
where
    'b: 'a,
    'c: 'a,
{
    let shortcut_ptr = match shortcut {
        Some(s) => s.as_ptr(),
        None => ptr::null(),
    };

    unsafe { igMenuItemBoolPtr(label.as_ptr(), shortcut_ptr, selected as *mut bool, enabled) }
}
