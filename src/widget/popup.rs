use crate::context::frame::Frame;
use imgui_sys::*;
use std::ffi::CStr;
use std::marker::PhantomData;
use std::ptr;

pub fn open_popup<'a, 'b>(_: &mut Frame<'a>, str_id: &'b CStr)
where
    'b: 'a,
{
    unsafe {
        igOpenPopup(str_id.as_ptr());
    }
}

pub struct PopupModal<'a> {
    open: bool,
    phantom: PhantomData<&'a ()>,
}

impl<'a> PopupModal<'a> {
    pub fn new<'b, 'c>(
        _: &mut Frame<'b>,
        name: &'c CStr,
        close_button: bool,
        flags: ImGuiWindowFlags,
    ) -> PopupModal<'a>
    where
        'c: 'b,
        'b: 'a,
    {
        let mut dummy = true;
        let dummy_ptr = if close_button {
            &mut dummy as *mut bool
        } else {
            ptr::null_mut()
        };

        PopupModal {
            open: unsafe { igBeginPopupModal(name.as_ptr(), dummy_ptr, flags) },
            phantom: PhantomData,
        }
    }

    /// Tells whether this is "open" or not, ie. the return value of the BeginPopupModal call.
    /// This is used to determine whether to start establishing contents.
    pub fn open(&self) -> bool {
        self.open
    }
}

impl<'a> Drop for PopupModal<'a> {
    fn drop(&mut self) {
        if self.open {
            unsafe {
                igEndPopup();
            }
        }
    }
}
