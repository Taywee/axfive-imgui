use crate::context::frame::Frame;
use imgui_sys::*;
use std::ffi::CStr;
use std::os::raw::c_int;

pub fn columns<'a, 'b>(_: &mut Frame<'a>, count: c_int, id: &'b CStr, border: bool)
where
    'b: 'a,
{
    unsafe {
        igColumns(count, id.as_ptr(), border);
    }
}

pub fn next_column(_: &mut Frame<'_>) {
    unsafe {
        igNextColumn();
    }
}
