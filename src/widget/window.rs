use crate::context::frame::Frame;
use imgui_sys::*;
use std::ffi::CStr;

pub struct Window {
    open: bool,
}

impl Window {
    pub fn new<'a, 'b>(
        _: &mut Frame<'a>,
        name: &'b CStr,
        p_open: &mut bool,
        flags: ImGuiWindowFlags,
    ) -> Window
    where
        'b: 'a,
    {
        Window {
            open: unsafe { igBegin(name.as_ptr(), p_open as *mut bool, flags) },
        }
    }

    /// Tells whether this is "open" or not, ie. the return value of the BeginWindow call.
    /// This is used to determine whether to start establishing contents.
    pub fn open(&self) -> bool {
        self.open
    }
}

impl Drop for Window {
    fn drop(&mut self) {
        unsafe {
            igEnd();
        }
    }
}

pub struct Child {
    open: bool,
}

impl Child {
    pub fn new<'a, 'b>(
        _: &mut Frame<'a>,
        str_id: &'b CStr,
        size: (f32, f32),
        border: bool,
        flags: ImGuiWindowFlags,
    ) -> Child
    where
        'b: 'a,
    {
        Child {
            open: unsafe {
                igBeginChild(
                    str_id.as_ptr(),
                    ImVec2 {
                        x: size.0,
                        y: size.1,
                    },
                    border,
                    flags,
                )
            },
        }
    }

    /// Tells whether this is "open" or not, ie. the return value of the BeginChild call.
    /// This is used to determine whether to start establishing contents.
    pub fn open(&self) -> bool {
        self.open
    }
}

impl Drop for Child {
    fn drop(&mut self) {
        unsafe {
            igEndChild();
        }
    }
}
