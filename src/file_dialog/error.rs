use std::convert::From;
use std::error;
use std::ffi::NulError;
use std::fmt;
use std::io;

#[derive(Debug)]
pub enum Error {
    IO(io::Error),
    Nul(NulError),
    NotDirectory,
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Error {
        Error::IO(e)
    }
}

impl From<NulError> for Error {
    fn from(e: NulError) -> Error {
        Error::Nul(e)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl error::Error for Error {}
