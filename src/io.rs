use clipboard::{ClipboardContext, ClipboardProvider};
use imgui_sys::{igGetIO, ImGuiIO};
use std::cell::RefCell;
use std::ffi::{c_void, CStr, CString, NulError};
use std::ops::{Deref, DerefMut};
use std::os::raw::{c_char, c_uint};
use std::pin::Pin;
use std::ptr;

pub struct IO {
    ptr: *mut ImGuiIO,
    backend_platform_name: CString,
    // We pin the box because we don't want the RefCell to move, and we want the RefCell in the
    // function so we can verify that we have only one mutable reference to the CString at a time
    clipboard_buffer: Pin<Box<RefCell<CString>>>,
}

extern "C" fn set_clipboard_text(_: *mut c_void, text: *const c_char) {
    let mut provider = ClipboardContext::new().unwrap();
    let string = unsafe { CStr::from_ptr(text) };
    provider
        .set_contents(string.to_str().unwrap().into())
        .unwrap();
}

extern "C" fn get_clipboard_text(user_data: *mut c_void) -> *const c_char {
    let mut provider = match ClipboardContext::new() {
        Ok(x) => x,
        Err(e) => {
            eprintln!("Error getting clipboard context: {}", e);
            return ptr::null();
        }
    };

    // Need a buffer to ensure that this outlasts this call.
    let ref_cell = user_data as *mut RefCell<CString>;
    let borrowed = unsafe { (*ref_cell).try_borrow_mut() };
    let mut buffer = match borrowed {
        Ok(x) => x,
        Err(e) => {
            eprintln!("Error borrowing the clipboard buffer: {}", e);
            return ptr::null();
        }
    };

    let contents = match provider.get_contents() {
        Ok(x) => x,
        Err(e) => {
            eprintln!("Error getting clipboard contents: {}", e);
            return ptr::null();
        }
    };
    *buffer = CString::new(contents).unwrap();
    buffer.as_ptr()
}

impl IO {
    pub fn get() -> IO {
        let mut io = IO {
            ptr: unsafe { igGetIO() },
            backend_platform_name: Default::default(),
            clipboard_buffer: Pin::new(Default::default()),
        };

        // Make sure the backend_platform_name is set to something
        io.set_backend_platform_name("DEFAULT").unwrap();

        // Set up clipboard to point at internal buffer
        io.ClipboardUserData =
            Pin::into_inner(io.clipboard_buffer.as_mut()) as *mut RefCell<CString> as *mut c_void;
        io.GetClipboardTextFn = Some(get_clipboard_text);
        io.SetClipboardTextFn = Some(set_clipboard_text);
        io
    }

    pub fn set_backend_platform_name<T: Into<Vec<u8>>>(&mut self, t: T) -> Result<(), NulError> {
        self.backend_platform_name = CString::new(t)?;
        self.BackendPlatformName = self.backend_platform_name.as_ptr();
        Ok(())
    }

    pub fn set_display_framebuffer_scale(&mut self, x: f32, y: f32) {
        self.DisplayFramebufferScale.x = x;
        self.DisplayFramebufferScale.y = y;
    }

    pub fn add_input_character(&mut self, c: char) {
        unsafe {
            imgui_sys::ImGuiIO_AddInputCharacter(self.ptr as *mut ImGuiIO, c as c_uint);
        }
    }
}

impl Drop for IO {
    fn drop(&mut self) {
        self.ClipboardUserData = std::ptr::null_mut();
        self.GetClipboardTextFn = None;
        self.SetClipboardTextFn = None;
    }
}

impl Deref for IO {
    type Target = ImGuiIO;
    fn deref(&self) -> &Self::Target {
        unsafe { &*self.ptr }
    }
}

impl DerefMut for IO {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.ptr }
    }
}
