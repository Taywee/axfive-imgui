use crate::context::Context;
use crate::events::Event as ImGuiEvent;
use glium::uniforms::{MagnifySamplerFilter, MinifySamplerFilter};
use glium::{uniform, Surface};
use imgui_sys;
use imgui_sys::ImDrawVert;
use std::default::Default;
use std::error::Error;
use std::f32;
use std::slice;

#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 2],
    uv: [f32; 2],
    color: [u8; 4],
}

glium::implement_vertex!(Vertex, position normalize(false), uv normalize(false), color normalize(true));

pub struct Frame<'a> {
    events: Vec<ImGuiEvent>,
    context: &'a Context,
    rendered: bool,
}

impl<'a> Frame<'a> {
    pub(super) fn new(events: Vec<ImGuiEvent>, context: &'a Context) -> Frame<'a> {
        Frame {
            events,
            context,
            rendered: false,
        }
    }

    pub fn render(
        mut self,
        display: &mut glium::Display,
        target: &mut glium::Frame,
    ) -> Result<(), Box<dyn Error>> {
        unsafe {
            imgui_sys::igRender();
        }
        self.rendered = true;

        // Render imgui stuff here
        let draw_data = unsafe { &*imgui_sys::igGetDrawData() };
        let fb_width = draw_data.DisplaySize.x * draw_data.FramebufferScale.x;
        let fb_height = draw_data.DisplaySize.y * draw_data.FramebufferScale.y;
        if fb_width > 0.0 && fb_height > 0.0 {
            let display_pos = draw_data.DisplayPos;
            let display_size = draw_data.DisplaySize;
            let l = display_pos.x;
            let r = display_pos.x + display_size.x;
            let t = display_pos.y;
            let b = display_pos.y + display_size.y;
            let ortho_projection: [[f32; 4]; 4] = [
                [2.0 / (r - l), 0.0, 0.0, 0.0],
                [0.0, 2.0 / (t - b), 0.0, 0.0],
                [0.0, 0.0, -1.0, 0.0],
                [(r + l) / (l - r), (t + b) / (b - t), 0.0, 1.0],
            ];
            let clip_offset = draw_data.DisplayPos;
            let clip_scale = draw_data.FramebufferScale;

            let cmd_lists: &mut [&mut imgui_sys::ImDrawList] = unsafe {
                slice::from_raw_parts_mut(
                    draw_data.CmdLists as *mut &mut imgui_sys::ImDrawList,
                    draw_data.CmdListsCount as usize,
                )
            };

            for cmd_list in cmd_lists {
                let cmd_buffer = &cmd_list.CmdBuffer;
                let idx_buffer = &cmd_list.IdxBuffer;
                let vtx_buffer = &cmd_list.VtxBuffer;

                let cmd_slice =
                    unsafe { slice::from_raw_parts_mut(cmd_buffer.Data, cmd_buffer.Size as usize) };
                let idx_slice =
                    unsafe { slice::from_raw_parts_mut(idx_buffer.Data, idx_buffer.Size as usize) };
                let vtx_slice: &[ImDrawVert] =
                    unsafe { slice::from_raw_parts_mut(vtx_buffer.Data, vtx_buffer.Size as usize) };

                let vertices: Vec<Vertex> = vtx_slice
                    .into_iter()
                    .map(|v| Vertex {
                        position: [v.pos.x, v.pos.y],
                        uv: [v.uv.x, v.uv.y],
                        color: v.col.to_ne_bytes(),
                    })
                    .collect();

                let vertex_buffer = glium::VertexBuffer::immutable(display, &vertices)?;
                let index_buffer = glium::IndexBuffer::immutable(
                    display,
                    glium::index::PrimitiveType::TrianglesList,
                    idx_slice,
                )?;
                for command in cmd_slice {
                    if let Some(_callback) = command.UserCallback {
                        // TODO: this crap here.  We probably don't need it if we stick to glium,
                        // because it manages state inside of the relevant objects.
                    } else {
                        // Project scissor/clipping rectangles into framebuffer space
                        // This rect is in terms of x1, y1, x2, y2
                        // Using an ImVec4 for this is misleading due to the z and w components.
                        let clip_rect = [
                            (command.ClipRect.x - clip_offset.x) * clip_scale.x,
                            (command.ClipRect.y - clip_offset.y) * clip_scale.y,
                            (command.ClipRect.z - clip_offset.x) * clip_scale.x,
                            (command.ClipRect.w - clip_offset.y) * clip_scale.y,
                        ];

                        if clip_rect[0] < fb_width
                            && clip_rect[1] < fb_height
                            && clip_rect[2] >= 0.0
                            && clip_rect[3] >= 0.0
                        {
                            let texture = unsafe { &*(command.TextureId as *mut glium::Texture2d) };

                            let vertex_slice =
                                vertex_buffer.slice((command.VtxOffset as usize)..).unwrap();
                            let vertex_slice: glium::vertex::VerticesSource<'_> =
                                vertex_slice.into();
                            let index_slice = index_buffer
                                .slice(
                                    (command.IdxOffset as usize)
                                        ..(command.IdxOffset as usize + command.ElemCount as usize),
                                )
                                .unwrap();

                            target.draw(vertex_slice, index_slice, &self.context.program, &uniform! {
                                    Texture: texture.sampled().minify_filter(MinifySamplerFilter::Linear).magnify_filter(MagnifySamplerFilter::Linear),
                                    ProjMtx: ortho_projection,
                                }, &glium::DrawParameters {
                                    blend: glium::Blend::alpha_blending(),
                                    scissor: Some(glium::Rect{
                                        left: f32::max(0.0, clip_rect[0]).floor() as u32,
                                        bottom: f32::max(0.0, fb_height - clip_rect[3]).floor() as u32,
                                        width: (clip_rect[2] - clip_rect[0]).abs().ceil() as u32,
                                        height: (clip_rect[3] - clip_rect[1]).abs().ceil() as u32,
                                    }),
                                    .. Default::default()
                                })?;
                        }
                    }
                }
            }
        }
        Ok(())
    }

    pub fn events(&self) -> std::slice::Iter<ImGuiEvent> {
        self.events.iter()
    }
}

impl Drop for Frame<'_> {
    fn drop(&mut self) {
        if !self.rendered {
            unsafe {
                imgui_sys::igEndFrame();
            }
        }
    }
}
