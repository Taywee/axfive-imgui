use std::ptr;

pub fn style_colors_light() {
    unsafe {
        imgui_sys::igStyleColorsLight(ptr::null_mut());
    }
}
