#version 130

uniform mat4 ProjMtx;
in vec2 position;
in vec2 uv;
in vec4 color;
out vec2 Frag_UV;
out vec4 Frag_Color;

void main() {
    Frag_UV = uv;
    Frag_Color = color;
    gl_Position = ProjMtx * vec4(position.xy,0,1);
}
