use imgui_sys::{igGetMouseCursor, ImGuiMouseCursor};

pub fn get_mouse_cursor() -> ImGuiMouseCursor {
    unsafe { igGetMouseCursor() }
}
