#[derive(Debug, Clone, Copy)]
pub enum Event {
    Save,
    SaveAs,
    Open,
    Quit,
}
