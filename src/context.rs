pub mod frame;

use crate::events::Event as ImGuiEvent;
use frame::Frame;
use glium::glutin;
use glium::glutin::{
    ElementState, Event, MouseButton, MouseCursor, MouseScrollDelta, VirtualKeyCode, WindowEvent,
};
use glium::texture::RawImage2d;
use imgui_sys;
use imgui_sys::{
    ImGuiBackendFlags_HasMouseCursors, ImGuiBackendFlags_RendererHasVtxOffset,
    ImGuiConfigFlags_NoMouseCursorChange, ImVec2,
};
use std::convert::TryInto;
use std::default::Default;
use std::error::Error;
use std::f32;
use std::os::raw::{c_int, c_uchar, c_void};
use std::pin::Pin;
use std::ptr;
use std::slice;

// Simple convenience struct to make things easier , and ensure that this drops correctly if the
// Context constructor fails
struct ContextWrapper(*mut imgui_sys::ImGuiContext);

impl ContextWrapper {
    pub fn new() -> ContextWrapper {
        ContextWrapper(unsafe { imgui_sys::igCreateContext(ptr::null_mut()) })
    }
}

impl Drop for ContextWrapper {
    fn drop(&mut self) {
        unsafe {
            imgui_sys::igDestroyContext(self.0);
        }
    }
}

pub struct Context {
    _context: ContextWrapper,
    // Just to keep it alive as long as the program is running
    _texture_atlas: Pin<Box<glium::Texture2d>>,
    program: glium::Program,
    io: crate::IO,
    mouse_pressed: u8,
}

impl Context {
    pub fn new(display: &glium::Display) -> Result<Context, Box<dyn Error>> {
        let _context = ContextWrapper::new();

        let vertex_shader_src = include_str!("vertex_shader.glsl");
        let fragment_shader_src = include_str!("fragment_shader.glsl");
        let program =
            glium::Program::from_source(display, vertex_shader_src, fragment_shader_src, None)?;

        crate::style_colors_light();

        // This also sets up clipboard functionality automatically
        let mut io = crate::IO::get();
        io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors as i32;
        io.BackendFlags |= ImGuiBackendFlags_RendererHasVtxOffset as i32;
        //io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos as i32;
        io.set_backend_platform_name("glutin")?;

        // Set keycode stuff
        for key_code in [
            imgui_sys::ImGuiKey_Tab,
            imgui_sys::ImGuiKey_LeftArrow,
            imgui_sys::ImGuiKey_RightArrow,
            imgui_sys::ImGuiKey_UpArrow,
            imgui_sys::ImGuiKey_DownArrow,
            imgui_sys::ImGuiKey_PageUp,
            imgui_sys::ImGuiKey_PageDown,
            imgui_sys::ImGuiKey_Home,
            imgui_sys::ImGuiKey_End,
            imgui_sys::ImGuiKey_Insert,
            imgui_sys::ImGuiKey_Delete,
            imgui_sys::ImGuiKey_Backspace,
            imgui_sys::ImGuiKey_Space,
            imgui_sys::ImGuiKey_Enter,
            imgui_sys::ImGuiKey_Escape,
            imgui_sys::ImGuiKey_KeyPadEnter,
            imgui_sys::ImGuiKey_A,
            imgui_sys::ImGuiKey_C,
            imgui_sys::ImGuiKey_V,
            imgui_sys::ImGuiKey_X,
            imgui_sys::ImGuiKey_Y,
            imgui_sys::ImGuiKey_Z,
        ]
        .into_iter()
        {
            io.KeyMap[*key_code as usize] = (*key_code).try_into()?;
        }

        let dpi = display.gl_window().window().get_hidpi_factor();

        unsafe {
            let mut font_bytes: Vec<u8> = include_bytes!("data/fonts/DroidSans.ttf")
                .into_iter()
                .cloned()
                .collect();
            let font_config = imgui_sys::ImFontConfig {
                FontDataOwnedByAtlas: false,
                OversampleH: 4,
                OversampleV: 4,
                GlyphMaxAdvanceX: f32::MAX,
                RasterizerMultiply: 1.0,
                //EllipsisChar: -1,
                ..Default::default()
            };
            imgui_sys::ImFontAtlas_AddFontFromMemoryTTF(
                io.Fonts,
                font_bytes.as_mut_ptr() as *mut c_void,
                font_bytes.len() as c_int,
                12.0 * dpi as f32,
                &font_config as *const imgui_sys::ImFontConfig,
                ptr::null(),
            );
        }

        // Init font atlas data
        let mut _texture_atlas = {
            let mut width: c_int = 0;
            let mut height: c_int = 0;
            let pixels: Vec<c_uchar> = unsafe {
                let mut pixels: *mut c_uchar = ptr::null_mut();
                let mut bytes_per_pixel: c_int = 0;

                imgui_sys::ImFontAtlas_GetTexDataAsRGBA32(
                    io.Fonts,
                    &mut pixels as *mut *mut c_uchar,
                    &mut width as *mut c_int,
                    &mut height as *mut c_int,
                    &mut bytes_per_pixel as *mut c_int,
                );

                slice::from_raw_parts_mut(pixels, (width * height * bytes_per_pixel) as usize)
                    .iter()
                    .cloned()
                    .collect()
            };

            let image = RawImage2d::from_raw_rgba(pixels, (width as u32, height as u32));
            Pin::new(Box::new(glium::Texture2d::new(display, image)?))
        };

        // Set the texture ID to be a mutable pointer to the texture atlas
        unsafe {
            imgui_sys::ImFontAtlas_SetTexID(
                io.Fonts,
                _texture_atlas.as_mut().get_mut() as *mut glium::Texture2d as *mut c_void,
            );
        }
        io.set_display_framebuffer_scale(dpi as f32, dpi as f32);

        Ok(Context {
            _context,
            program,
            io,
            _texture_atlas,
            mouse_pressed: 0,
        })
    }

    pub fn start_frame<'a>(
        &'a mut self,
        events_loop: &mut glutin::EventsLoop,
        display: &mut glium::Display,
    ) -> Frame<'a> {
        // Special handling for mouse events that may be shorter than a frame.  If a button is clicked
        // and released in under a frame, it won't otherwise register as a click.
        let mut mouse_pressed_this_frame = 0u8;
        let mut events = Vec::new();
        // listing the events produced by application and waiting to be received
        events_loop.poll_events(|ev| {
            match ev {
                Event::WindowEvent { event, .. } => {
                    match event {
                        WindowEvent::CloseRequested => events.push(ImGuiEvent::Quit),
                        WindowEvent::Resized(size) => {
                            self.io.DisplaySize = ImVec2 {
                                x: size.width as f32,
                                y: size.height as f32,
                            };
                        }
                        WindowEvent::KeyboardInput { input, .. } => {
                            let down = input.state == ElementState::Pressed;

                            let index = match input.virtual_keycode {
                                Some(VirtualKeyCode::Tab) => Some(imgui_sys::ImGuiKey_Tab),
                                Some(VirtualKeyCode::Left) => Some(imgui_sys::ImGuiKey_LeftArrow),
                                Some(VirtualKeyCode::Right) => Some(imgui_sys::ImGuiKey_RightArrow),
                                Some(VirtualKeyCode::Up) => Some(imgui_sys::ImGuiKey_UpArrow),
                                Some(VirtualKeyCode::Down) => Some(imgui_sys::ImGuiKey_DownArrow),
                                Some(VirtualKeyCode::PageUp) => Some(imgui_sys::ImGuiKey_PageUp),
                                Some(VirtualKeyCode::PageDown) => {
                                    Some(imgui_sys::ImGuiKey_PageDown)
                                }
                                Some(VirtualKeyCode::Home) => Some(imgui_sys::ImGuiKey_Home),
                                Some(VirtualKeyCode::End) => Some(imgui_sys::ImGuiKey_End),
                                Some(VirtualKeyCode::Insert) => Some(imgui_sys::ImGuiKey_Insert),
                                Some(VirtualKeyCode::Delete) => Some(imgui_sys::ImGuiKey_Delete),
                                Some(VirtualKeyCode::Back) => Some(imgui_sys::ImGuiKey_Backspace),
                                Some(VirtualKeyCode::Space) => Some(imgui_sys::ImGuiKey_Space),
                                Some(VirtualKeyCode::Return) => Some(imgui_sys::ImGuiKey_Enter),
                                Some(VirtualKeyCode::Escape) => Some(imgui_sys::ImGuiKey_Escape),
                                Some(VirtualKeyCode::NumpadEnter) => {
                                    Some(imgui_sys::ImGuiKey_KeyPadEnter)
                                }
                                Some(VirtualKeyCode::A) => Some(imgui_sys::ImGuiKey_A),
                                Some(VirtualKeyCode::C) => Some(imgui_sys::ImGuiKey_C),
                                Some(VirtualKeyCode::V) => Some(imgui_sys::ImGuiKey_V),
                                Some(VirtualKeyCode::X) => Some(imgui_sys::ImGuiKey_X),
                                Some(VirtualKeyCode::Y) => Some(imgui_sys::ImGuiKey_Y),
                                Some(VirtualKeyCode::Z) => Some(imgui_sys::ImGuiKey_Z),
                                // Not recognized
                                _ => None,
                            };
                            if let Some(index) = index {
                                self.io.KeysDown[index as usize] = down;
                            }

                            if down {
                                match (
                                    input.modifiers.ctrl,
                                    input.modifiers.shift,
                                    input.modifiers.alt,
                                    input.virtual_keycode,
                                ) {
                                    (true, true, false, Some(VirtualKeyCode::S)) => {
                                        events.push(ImGuiEvent::SaveAs)
                                    }
                                    (true, false, false, Some(VirtualKeyCode::S)) => {
                                        events.push(ImGuiEvent::Save)
                                    }
                                    (true, false, false, Some(VirtualKeyCode::O)) => {
                                        events.push(ImGuiEvent::Open)
                                    }
                                    (true, false, false, Some(VirtualKeyCode::Q)) => {
                                        events.push(ImGuiEvent::Quit)
                                    }
                                    _ => (),
                                }
                            }

                            self.io.KeyShift = input.modifiers.shift;
                            self.io.KeyCtrl = input.modifiers.ctrl;
                            self.io.KeyAlt = input.modifiers.alt;
                            self.io.KeySuper = input.modifiers.logo;
                        }
                        WindowEvent::ReceivedCharacter(c) => {
                            self.io.add_input_character(c);
                        }
                        WindowEvent::CursorMoved { position, .. } => {
                            self.io.MousePos = ImVec2 {
                                x: position.x as f32,
                                y: position.y as f32,
                            };
                        }
                        WindowEvent::MouseWheel { delta, .. } => {
                            match delta {
                                MouseScrollDelta::LineDelta(x, y) => {
                                    // the input variables are lines, but self.io.MouseWheel has 1 unit
                                    // about 5 lines of text.
                                    self.io.MouseWheel += y / 5.0;
                                    self.io.MouseWheelH += x / 5.0;
                                }
                                MouseScrollDelta::PixelDelta(position) => {
                                    // the input variables are pixels, but self.io.MouseWheel has 1 unit
                                    // for about 5 lines of text.
                                    self.io.MouseWheel += (position.y * 3.0) as f32;
                                    self.io.MouseWheelH += (position.x * 3.0) as f32;
                                }
                            }
                        }
                        WindowEvent::MouseInput { state, button, .. } => {
                            let button: usize = match button {
                                MouseButton::Left => 0,
                                MouseButton::Right => 1,
                                MouseButton::Middle => 2,
                                MouseButton::Other(i) => i.into(),
                            };
                            if button < 5 {
                                let bit = 1u8 << button;
                                match state {
                                    ElementState::Pressed => {
                                        self.mouse_pressed |= bit;
                                        mouse_pressed_this_frame |= bit;
                                    }
                                    ElementState::Released => {
                                        self.mouse_pressed &= !bit;
                                    }
                                }
                            }
                        }
                        _ => (),
                    }
                }
                _ => (),
            }
        });

        // Update the mouse buttons
        for button in 0..5 {
            let bit = 1u8 << button;
            self.io.MouseDown[button] =
                ((self.mouse_pressed | mouse_pressed_this_frame) & bit) != 0;
        }

        // Now we update the mouse cursor
        if self.io.ConfigFlags as u32 & ImGuiConfigFlags_NoMouseCursorChange == 0 {
            let cursor = crate::get_mouse_cursor();
            // hide the cursor if imgui requests no cursor or wants to draw it itself.
            let hide_cursor = cursor == imgui_sys::ImGuiMouseCursor_None || self.io.MouseDrawCursor;
            display.gl_window().window().hide_cursor(hide_cursor);
            if !hide_cursor {
                let dest_cursor = match cursor {
                    imgui_sys::ImGuiMouseCursor_Arrow => MouseCursor::Arrow,
                    imgui_sys::ImGuiMouseCursor_Hand => MouseCursor::Hand,
                    imgui_sys::ImGuiMouseCursor_ResizeAll => MouseCursor::Arrow,
                    imgui_sys::ImGuiMouseCursor_ResizeEW => MouseCursor::EwResize,
                    imgui_sys::ImGuiMouseCursor_ResizeNESW => MouseCursor::NeswResize,
                    imgui_sys::ImGuiMouseCursor_ResizeNS => MouseCursor::NsResize,
                    imgui_sys::ImGuiMouseCursor_ResizeNWSE => MouseCursor::NwseResize,
                    imgui_sys::ImGuiMouseCursor_TextInput => MouseCursor::Text,
                    // Apparently not there yet?
                    //imgui_sys::ImGuiMouseCursor_NotAllowed => MouseCursor::NotAllowed,
                    _ => MouseCursor::NotAllowed,
                };

                display.gl_window().window().set_cursor(dest_cursor);
            }
        }

        unsafe {
            imgui_sys::igNewFrame();
        }

        Frame::new(events, self)
    }
}
