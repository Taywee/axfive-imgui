use crate::context::frame::Frame;
use crate::widget;
use std::convert::AsRef;
use std::convert::Into;
use std::default::Default;
use std::ffi::CString;
use std::iter::DoubleEndedIterator;
use std::path::{Component, Path, PathBuf, MAIN_SEPARATOR};

pub mod error;

pub use self::error::Error;

/// The necessary information for storing, rendering, and working with each individual button in
/// the working directory path
#[derive(Default, Debug)]
struct ComponentButton {
    text: CString,
    path: PathBuf,
    separator: bool,
}

#[derive(Default, Debug)]
pub struct FileDialog {
    path_buttons: Vec<ComponentButton>,
    main_separator: String,
    dir: PathBuf,
    changed_dir: bool,
    title: CString,
}

impl FileDialog {
    pub fn new<P>(path: P) -> Result<FileDialog, Error>
    where
        P: Into<PathBuf>,
    {
        let path_buf = path.into().canonicalize()?;
        if !path_buf.is_dir() {
            return Err(Error::NotDirectory);
        }
        let main_separator = format!(" {} ", MAIN_SEPARATOR);
        Ok(FileDialog {
            dir: path_buf,
            changed_dir: true,
            main_separator,
            title: CString::new("Test File Dialog Window")?,
            ..Default::default()
        })
    }

    pub fn render<'a, 'b>(&'a mut self, frame: &mut Frame<'b>, show: bool)
    where
        'a: 'b,
    {
        if show {
            widget::popup::open_popup(frame, &self.title);
        }

        let modal = widget::popup::PopupModal::new(frame, &self.title, true, 0);
        if modal.open() {
            // If the directory is changed, rebuild all the buttons
            if self.changed_dir {
                let mut working_path = PathBuf::new();

                self.path_buttons.clear();
                for (index, component) in self.dir.components().enumerate() {
                    let component_path: &Path = component.as_ref();
                    working_path.push(component);
                    self.path_buttons.push(ComponentButton {
                        // Ensure the id is unique
                        text: CString::new(format!("{}##{}", component_path.display(), index))
                            .expect("This should never happen"),
                        path: working_path.clone(),
                        separator: match component {
                            Component::RootDir | Component::Prefix(_) => false,
                            _ => true,
                        },
                    });
                }

                self.changed_dir = false;
            }

            // Render all the buttons for this path
            let mut iter = self.path_buttons.iter();
            if let Some(last) = iter.next_back() {
                for component in iter {
                    if widget::button(frame, &component.text, (0.0, 0.0)) {
                        self.dir = component.path.clone();
                        self.changed_dir = true;
                    }
                    widget::same_line(frame, 0.0, 0.0);
                    if component.separator {
                        widget::text(frame, &self.main_separator);
                    } else {
                        widget::text(frame, " ");
                    }
                    widget::same_line(frame, 0.0, 0.0);
                }
                if widget::button(frame, &last.text, (0.0, 0.0)) {
                    self.dir = last.path.clone();
                    self.changed_dir = true;
                }
            }
        }
    }
}
