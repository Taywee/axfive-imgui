use crate::context::frame::Frame;
use imgui_sys::*;
use std::ffi::CStr;
use std::os::raw::c_char;

pub mod columns;
pub mod input;
pub mod menu;
pub mod popup;
pub mod window;

pub fn text<'a, 'b>(_: &mut Frame<'a>, content: &'b str)
where
    'b: 'a,
{
    let beginning = content.as_ptr() as *const c_char;
    unsafe {
        let end = beginning.add(content.len());
        igTextUnformatted(beginning, end);
    }
}

pub fn separator(_: &mut Frame<'_>) {
    unsafe {
        igSeparator();
    }
}

pub fn drag_float<'a, 'b, 'c>(
    _: &mut Frame<'a>,
    label: &'b CStr,
    v: &mut f32,
    v_speed: f32,
    v_min: f32,
    v_max: f32,
    format: &'c CStr,
    power: f32,
) -> bool
where
    'b: 'a,
    'c: 'a,
{
    unsafe {
        igDragFloat(
            label.as_ptr(),
            v as *mut f32,
            v_speed,
            v_min,
            v_max,
            format.as_ptr(),
            power,
        )
    }
}

pub fn button<'a, 'b>(_: &mut Frame<'a>, label: &'b CStr, size: (f32, f32)) -> bool
where
    'b: 'a,
{
    unsafe {
        imgui_sys::igButton(
            label.as_ptr(),
            ImVec2 {
                x: size.0,
                y: size.1,
            },
        )
    }
}

pub fn same_line(_: &mut Frame<'_>, offset_from_start_x: f32, spacing: f32) {
    unsafe {
        igSameLine(offset_from_start_x, spacing);
    }
}
