pub mod context;
pub mod cursor;
pub mod events;
pub mod file_dialog;
pub mod io;
pub mod style;
pub mod widget;

pub use context::Context;
pub use cursor::get_mouse_cursor;
pub use file_dialog::FileDialog;
pub use io::IO;
pub use style::style_colors_light;
pub use widget::columns::{columns, next_column};
pub use widget::input::input_text;
pub use widget::menu::{menu_item, menu_item_toggle, MainMenuBar, Menu};
pub use widget::popup::{open_popup, PopupModal};
pub use widget::window::{Child, Window};
pub use widget::{button, drag_float, same_line, separator, text};

pub use glium;
pub use imgui_sys;

pub fn show_demo_window(running: &mut bool) {
    unsafe {
        imgui_sys::igShowDemoWindow(running as *mut bool);
    }
}

#[macro_export]
/// Simple macro to turn a string literal into a &'static CStr without having to worry about null
/// bytes.
macro_rules! static_cstr {
    ($string:literal) => {
        unsafe { std::ffi::CStr::from_bytes_with_nul_unchecked(concat!($string, '\0').as_bytes()) }
    };
}
